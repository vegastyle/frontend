import React from "react";
import Link from "next/link";

const OutgoingBlock = ({ url, title, text, link }) => {
  return (
    <div className="outgoing_block">
      <div className="outgoing_block_info">
        <div className="outgoing_block_info_title">
          <h3>{title}</h3>
        </div>
        <div className="outgoing_block_info_text">
          <Link href={link}>
            <a className="sklad-link">{text}</a>
          </Link>
        </div>
      </div>
      <img src={url} alt="image" className="outgoing_block_img"></img>
      <img
        src={`/img/pattern_coop.png`}
        alt="image"
        className="outgoing_block_pattern"
      ></img>
    </div>
  );
};

export default OutgoingBlock;

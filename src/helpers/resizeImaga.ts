import { api } from "../constant/api";

export enum TYPE_IMAGE {
  LARGE = "large",
  MEDIUM = "medium",
  SMALL = "small",
}

export const resizeImage = (
  type: TYPE_IMAGE,
  item: { ext: string; hash: string; url: string }
) => {
  if (item.ext === ".svg") {
    return `${api}${item.url}`;
  }
  if (item.ext === ".gif") {
    return `${api}${item.url}`;
  }
  return `${api}/uploads/${type}_${item.hash}${item.ext}`;
};

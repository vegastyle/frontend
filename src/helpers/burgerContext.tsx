import React, { FC, useContext, useState } from "react";

type Props = {
  open: boolean;
  toggle: () => void;
};

// @ts-ignore
export const ManageBurger: React.Context<Props> = React.createContext({
  open: false,
  toggle: () => {},
});

export const useBurger = () => useContext<Props>(ManageBurger);

export const ContextBugger: FC = ({ children }) => {
  const [open, setOpen] = useState(false);

  const toggle = () => {
    setOpen((s) => !s);
  };

  return (
    <ManageBurger.Provider
      value={{
        open,
        toggle,
      }}
    >
      {children}
    </ManageBurger.Provider>
  );
};

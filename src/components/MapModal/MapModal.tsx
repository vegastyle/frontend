import React, { FC } from "react";
import Modal from "react-modal";
import { YMaps, Map, Placemark } from "react-yandex-maps";

type Props = {
  state: boolean;
  latitude: number;
  long: number;
  closeModal: () => void;
};
const customStyles = {
  content: {
    width: 800,
    height: 450,
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

const MapModal: FC<Props> = ({ state, closeModal, latitude, long }) => {
  return (
    <Modal
      isOpen={state}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel="Example Modal"
      ariaHideApp={false}
    >
      <YMaps>
        <Map
          width={"100%"}
          height={"100%"}
          defaultState={{ center: [long, latitude], zoom: 16 }}
        >
          <Placemark geometry={[long, latitude]} />
        </Map>
      </YMaps>
    </Modal>
  );
};

export default MapModal;

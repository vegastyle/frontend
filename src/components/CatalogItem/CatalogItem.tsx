import React, { useState } from "react";
import Link from "next/link";
import { resizeImage, TYPE_IMAGE } from "../../helpers/resizeImaga";

const CatalogItem = ({ item, active, addFavorite }) => {
  const [currentImage, setCurrentImage] = useState(
    resizeImage(TYPE_IMAGE.SMALL, item.images[0])
  );
  return (
    <div className="catalog-item col-8 col-lg-4 col-sm-6 col-md-4 catalog-item-js">
      <Link href={`/detail/${item.id}`}>
        <div
          className="catalog-item-img"
          style={{
            backgroundImage: `url(${currentImage})`,
          }}
        >
          {/* <div className="js-hover-slider">
          {item.images.map((el, index) => {
            return (
              <div
                key={index}
                onMouseLeave={() =>
                  setCurrentImage(resizeImage(TYPE_IMAGE.SMALL, item.images[0]))
                }
                onMouseEnter={() =>
                  setCurrentImage(resizeImage(TYPE_IMAGE.SMALL, el))
                }
                className="js-hover-slider-item"
              />
            );
          })}
        </div> */}
        </div>
      </Link>
      <div className="pt-3 d-flex justify-content-center">
        <div className="d-flex align-items-center justify-content-between">
          <div className="catalog-item-name">{item.name}</div>
        </div>
        <div className="catalog-hidden-block" style={{ margin: "35px 0 0 0" }}>
          {item.dimensions ? (
            <div className="catalog-hidden-block-item mb-3">
              <div className="catalog-hidden-block-title">Размеры</div>
              <div className="catalog-hidden-dimensions-block d-flex justify-content-between">
                {item.dimensions &&
                  item.dimensions.size &&
                  item.dimensions.size.long && (
                    <div className="d-flex flex-column">
                      <div className="catalog-hidden-dimensions-title">
                        Длинна
                      </div>
                      <div className="catalog-hidden-dimensions-text">
                        {`${item.dimensions.size.long}`} см
                      </div>
                    </div>
                  )}
                {item.dimensions &&
                  item.dimensions.size &&
                  item.dimensions.size.width && (
                    <div className="d-flex flex-column">
                      <div className="catalog-hidden-dimensions-title">
                        Ширина
                      </div>
                      <div className="catalog-hidden-dimensions-text">
                        {`${item.dimensions.size.width}`} см
                      </div>
                    </div>
                  )}
                {item.dimensions &&
                  item.dimensions.size &&
                  item.dimensions.size.height && (
                    <div className="d-flex flex-column">
                      <div className="catalog-hidden-dimensions-title">
                        Высота
                      </div>
                      <div className="catalog-hidden-dimensions-text">
                        {`${item.dimensions.size.height}`} см
                      </div>
                    </div>
                  )}
              </div>
            </div>
          ) : null}
          {item.dimensions ? (
            <div className="catalog-hidden-block-item mb-3">
              <div className="catalog-hidden-block-title">Спальное место</div>
              <div className="catalog-hidden-dimensions-block d-flex justify-content-between">
                {item.dimensions &&
                  item.dimensions.sleepingPlace &&
                  item.dimensions.sleepingPlace.long && (
                    <div className="d-flex flex-column">
                      <div className="catalog-hidden-dimensions-title">
                        Длинна
                      </div>
                      <div className="catalog-hidden-dimensions-text">
                        {`${item.dimensions.sleepingPlace.long}`} см
                      </div>
                    </div>
                  )}
                {item.dimensions &&
                  item.dimensions.sleepingPlace &&
                  item.dimensions.sleepingPlace.width && (
                    <div className="d-flex flex-column">
                      <div className="catalog-hidden-dimensions-title">
                        Ширина
                      </div>
                      <div className="catalog-hidden-dimensions-text">
                        {`${item.dimensions.sleepingPlace.width}`} см
                      </div>
                    </div>
                  )}
                {item.dimensions &&
                  item.dimensions.sleepingPlace &&
                  item.dimensions.sleepingPlace.height && (
                    <div className="d-flex flex-column">
                      <div className="catalog-hidden-dimensions-title">
                        Высота
                      </div>
                      <div className="catalog-hidden-dimensions-text">
                        {`${item.dimensions.sleepingPlace.height}`} см
                      </div>
                    </div>
                  )}
              </div>
            </div>
          ) : null}
          <div className="d-flex flex-column mb-3 align-items-center">
            {item.price !== null ? (
              <div className="catalog-hidden-dimensions-text price-catalog">
                <span className={"prefix mr-2"}>от</span>
                {`${item.price}`}&#8381;
              </div>
            ) : (
              <span className={"prefix mr-2"}>Цена скоро появится</span>
            )}
          </div>
          <div className="catalog-hidden-block-btn mb-3">
            <Link href={`/detail/${item.id}`}>
              <a>Узнать больше</a>
            </Link>
          </div>
          <div
            className={`d-flex justify-content-center align-items-center favorite-link ${
              active ? "active" : ""
            }`}
            onClick={() => addFavorite(item.id)}
          >
            <svg
              id="color"
              enableBackground="new 0 0 24 24"
              height="24"
              viewBox="0 0 24 24"
              width="24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="m23.363 8.584-7.378-1.127-3.307-7.044c-.247-.526-1.11-.526-1.357 0l-3.306 7.044-7.378 1.127c-.606.093-.848.83-.423 1.265l5.36 5.494-1.267 7.767c-.101.617.558 1.08 1.103.777l6.59-3.642 6.59 3.643c.54.3 1.205-.154 1.103-.777l-1.267-7.767 5.36-5.494c.425-.436.182-1.173-.423-1.266z"
                fill={!active ? "white" : "#C2811D"}
                stroke="#C2811D"
              />
            </svg>
            <span className="ml-3 orange-text">В избранноe</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CatalogItem;

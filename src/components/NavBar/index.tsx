import React, { useState } from "react";
import Link from "next/link";

type NavBarProps = {
  config: { id: number; name: string; path: string }[];
  activeRoute: string;
};

const RenderNarBarItem = () => {};

const NavBar = ({ config, activeRoute }) => {
  const [showDropDown, setShowDropDown] = useState(false);
  return (
    <nav className="header__nav d-flex align-items-center">
      {config.map((item) => {
        if (item.children) {
          return (
            <li
              key={item.id}
              className="nav-item dropdown "
              onMouseEnter={() => setShowDropDown(true)}
              onMouseLeave={() => setShowDropDown(false)}
            >
              <Link href={`/${item.path}`}>
                <a className={"dropdown-custom nav-bar-link"}>
                  {item.name}
                  <svg
                    version="1.1"
                    id="Capa_1"
                    className={`ml-2 transition ${
                      showDropDown ? "rot-180" : ""
                    }`}
                    xmlns="http://www.w3.org/2000/svg"
                    x="0px"
                    y="0px"
                    width="14px"
                    height="14px"
                    viewBox="0 0 451.847 451.847"
                  >
                    <g>
                      <path
                        fill="#023838"
                        d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751
		c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0
		c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"
                      />
                    </g>
                  </svg>
                </a>
              </Link>
              <div
                className="dropdown-block"
                style={showDropDown ? { margin: "-10px 0 0 0" } : null}
              >
                {item.children.map((el, index) => {
                  const b = item.children.filter((el) => el.id === index + 1);

                  return (
                    <Link key={index} href={`/${b[0].path}`}>
                      <a
                        style={{ fontSize: "16px" }}
                        className="dropdown-block-link"
                      >
                        {b[0].name}
                      </a>
                    </Link>
                  );
                })}
              </div>
            </li>
          );
        } else {
          return (
            <li
              key={item.id}
              className={activeRoute === `/${item.path}` ? "active" : ""}
            >
              <Link href={`/${item.path}`}>
                <a className="nav-bar-link">{item.name}</a>
              </Link>
            </li>
          );
        }
      })}
    </nav>
  );
};

export default NavBar;

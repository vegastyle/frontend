import React, { useState } from "react";

import MainForm from "src/components/MainForm/MainForm";

import Modal from "react-modal";

import { api } from "src/constant/api";

type BuyModalProps = {
  mainPageData: any;
  isOpen: boolean;
  setOpenModal: (isOpen: boolean) => void;
};

const BuyModal: React.FC<BuyModalProps> = ({
  mainPageData,
  isOpen,
  setOpenModal,
}) => {
  const [formData, setFormData] = useState({
    name: "",
    lastName: "",
    number: "",
    email: "",
  });
  const [statusForm, setStatusForm] = useState({
    success: false,
    error: false,
  });
  const sendForm = () => {
    if (formData.number && formData.name) {
      fetch(`${api}/feedback-forms`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      }).then(() => {
        setFormData({ name: "", lastName: "", number: "", email: "" });
        setStatusForm((s) => ({
          ...s,
          success: true,
        }));
        setTimeout(() => {
          setStatusForm((s) => ({
            ...s,
            success: false,
          }));
        }, 3000);
      });
    }
  };

  const changeDataForm = (key, value) => {
    setFormData((s) => {
      return {
        ...s,
        [key]: value,
      };
    });
  };

  const customStyles = {
    content: {
      width: 1000,
      height: "auto",
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      padding: "0",
      position: "relative",
    },
  };
  return (
    <div className="buyModal">
      <Modal
        isOpen={isOpen}
        //  onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
        ariaHideApp={false}
      >
        <button
          onClick={() => setOpenModal(false)}
          className="buyModal_button_close"
        >
          Закрыть
        </button>
        <MainForm
          statusForm={statusForm}
          value={formData}
          submitForm={sendForm}
          changeDataForm={changeDataForm}
          imgPath={mainPageData.formBackgroundImage.url}
          title={mainPageData.formTitle}
          description={mainPageData.formDescription}
        />
      </Modal>
    </div>
  );
};

export async function getServerSideProps() {
  const mainPage = await fetch(`${api}/main-page-2`);

  const mainPageData = await mainPage.json();

  return {
    props: {
      mainPageData,
    },
  };
}
export default BuyModal;

import React, {FC} from 'react'

type props = {
    title: string
    description: string
}

const SmallBanner:FC<props> = ({title, description}) => {
    return (
        <section className="banner mb-0">
            <div className="container">
                <div className="d-flex flex-column banner-title-block">
                    <div className="banner-title">{title}</div>
                    <div className="banner-description">{description}</div>
                </div>
             </div>
    </section>
    )
}

export default SmallBanner

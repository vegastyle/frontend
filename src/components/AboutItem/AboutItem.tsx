import React, { FC } from "react";

const AboutItem: FC<any> = ({
  title,
  description,
  imagePath,
  reverse,
  index,
}) => {
  return (
    <div
      className={`reviews d-flex row justify-content-between col-xl-12 align-items-center ${
        reverse ? "flex-row-reverse" : ""
      }`}
    >
      <div className="reviews-text-block flex-column d-flex col-lg-12 col-xl-6">
        <div className="reviews-title">{title}</div>
        <div className="reviews-text ">{description}</div>
      </div>
      <div className="reviews-img-block col-lg-12 col-xl-6 d-flex align-items-center justify-content-lg-center">
        <img src={imagePath} alt="" />
      </div>
    </div>
  );
};

export default AboutItem;

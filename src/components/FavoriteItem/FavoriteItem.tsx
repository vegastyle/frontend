import React, { useState } from "react";
import { useFavorite } from "../../helpers/favoriteContext";
import { resizeImage, TYPE_IMAGE } from "../../helpers/resizeImaga";
import { useRouter } from "next/router";
import Link from "next/link";

const FavoriteItem = ({ item }) => {
  const [currentImage, setCurrentImage] = useState(
    resizeImage(TYPE_IMAGE.MEDIUM, item.images[0])
  );

  const { deleteItemFromFavorite } = useFavorite();
  const { push } = useRouter();
  return (
    <div className="favorite-page-item col-lg-12 catalog-item-js mb-5 d-flex">
      <div className="d-flex flex-column w-25 align-items-center">
        <div
          className="catalog-item-img mb-5"
          style={{
            backgroundImage: `url(${currentImage})`,
          }}
        >
          <div className="js-hover-slider">
            {item.images.map((el, index) => {
              return (
                <div
                  key={index}
                  onMouseEnter={() =>
                    setCurrentImage(resizeImage(TYPE_IMAGE.MEDIUM, el))
                  }
                  className="js-hover-slider-item"
                />
              );
            })}
          </div>
        </div>
        <Link href={`/detail/${item.id}`}>
          <a className="favorite-page-block-title">{item.name}</a>
        </Link>
      </div>

      <div className="d-flex w-100 justify-content-around">
        {item.dimensions && (
          <div className="favorite-page-block-item">
            <div className="favorite-page-block-title">Размеры</div>
            <div className="favorite-page-dimensions-block d-flex justify-content-between">
              {item.dimensions &&
                item.dimensions.size &&
                item.dimensions.size.long && (
                  <div className="d-flex flex-column">
                    <div className="favorite-page-dimensions-title">Длинна</div>
                    <div className="favorite-page-dimensions-text">
                      {`${item.dimensions.size.long}`} см
                    </div>
                  </div>
                )}
              {item.dimensions &&
                item.dimensions.size &&
                item.dimensions.size.width && (
                  <div className="d-flex flex-column">
                    <div className="favorite-page-dimensions-title">Ширина</div>
                    <div className="favorite-page-dimensions-text">
                      {`${item.dimensions.size.width}`} см
                    </div>
                  </div>
                )}
              {item.dimensions &&
                item.dimensions.size &&
                item.dimensions.size.height && (
                  <div className="d-flex flex-column">
                    <div className="favorite-page-dimensions-title">Высота</div>
                    <div className="favorite-page-dimensions-text">
                      {`${item.dimensions.size.height}`} см
                    </div>
                  </div>
                )}
            </div>
          </div>
        )}

        <div className="favorite-page-block-item">
          <div className="favorite-page-block-title">Спальное место</div>
          <div className="favorite-page-dimensions-block d-flex justify-content-between">
            <div className="d-flex flex-column mr-3">
              <div className="favorite-page-dimensions-title">Длинна</div>
              <div className="favorite-page-dimensions-text">218 см</div>
            </div>
            <div className="d-flex flex-column mr-3">
              <div className="favorite-page-dimensions-title">Ширина</div>
              <div className="favorite-page-dimensions-text">218 см</div>
            </div>
          </div>
        </div>
        <div className="d-flex flex-column">
          <div
            className="favorite-page-btns mb-5  d-flex flex-column justify-content-end"
            onClick={() => push(`/detail/${item.id}`)}
          >
            <a className="btn">Перейти</a>
          </div>
          <div
            className="favorite-page-btns d-flex flex-column justify-content-end"
            onClick={() => deleteItemFromFavorite(item.id)}
          >
            <a className="btn">Удалить</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FavoriteItem;

import React, { FC } from "react";
import Slider from "react-slick";
import { resizeImage, TYPE_IMAGE } from "../../helpers/resizeImaga";
import Link from "next/link";

type BannerProps = {
  title: string;
  description: string;

  sliders?: any[];

  withOutCarousel?: boolean;
  noneMB?: boolean;
  Banner_item: Banner_item_type[];
};

export type Banner_item_type = {
  Banner_Image: {};
  State: string;
  detail_item: {
    id: 24;
    name: string;
    price: null;
    color: [];
    images: { ext: string; hash: string; url: string }[];
  };
  id: 2;
};

const settings = {
  dots: false,
  infinite: true,
  arrows: false,
  speed: 1000,
  slidesToShow: 1,
  slidesToScroll: 1,
  swipeToSlide: false,
  autoPlay: true,
  vertical: false,
  autoplaySpeed: 5000,
  verticalSwiping: false,
  autoplay: true,
};

const Banner: FC<BannerProps> = ({
  title,
  description,
  sliders,
  withOutCarousel = false,
  noneMB = false,
  Banner_item,
}) => {
  console.log(Banner_item);
  const itemArray = [1, 2];
  return (
    <section className={`banner ${noneMB ? "mb-0" : ""}`}>
      <div className="slider_section">
        <div className="container">
          <div
            className="flex-container flex-column justify-content-center"
            style={{ height: "100%" }}
          >
            {/* <div
              className="first_block text-center "
              style={{ margin: "20px 0 0 0 " }}
            >
              <h3 className="title_banner">{title}</h3>
              <p style={{ padding: "15px 0 0 0" }}>{description}</p>
            </div> */}

            <div className="first_block text-center object banner_arrow">
              <div className={"d-flex flex-column"}>
                <svg
                  version="1.1"
                  id="Capa_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="25px"
                  height="25px"
                  viewBox="0 0 451.847 451.847"
                >
                  <g>
                    <path
                      fill="#000"
                      d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751
		c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0
		c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"
                    />
                  </g>
                </svg>
              </div>
            </div>
          </div>
        </div>
        {!withOutCarousel && sliders && (
          <div
            className="second_block"
            // style={{
            //   maxWidth: "974px",
            // }}
          >
            <Slider {...settings}>
              {itemArray.map((item, index) => {
                return (
                  <div key={String(index)} className={"banner_container"}>
                    <div className="position-relaitve">
                      <div>
                        <div className="position-relative">
                          {/* {item.Banner_Image[0] ? ( */}
                          <img
                            src={`/img/${itemArray[index]}.gif`}
                            // {resizeImage(
                            //   TYPE_IMAGE.MEDIUM,
                            // item.Banner_Image[0]

                            //   )
                            // }
                            alt=""
                          />
                          {/* ) : (
                            <img
                              src={resizeImage(
                                TYPE_IMAGE.MEDIUM,
                                item.detail_item.images[0]
                              )}
                              alt=""
                            />
                          )} */}

                          {/* <div className="banner_discription">
                            <div className="discription">
                              <div> {item.detail_item.name}</div>
                              <div>
                                {item.detail_item.price
                                  ? item.detail_item.price
                                  : "Цена врменно отсутствует "}
                              </div>
                              <Link href={`/detail/${item.detail_item.id}`}>
                                <div>
                                  <button>Подробнее</button>
                                </div>
                              </Link>
                            </div>
                          </div>
                          <div className="banner_sale">
                            {item.State === "Sale" ? (
                              <img src={"/img/sale.png"} />
                            ) : null}
                            {item.State === "Stoke" ? (
                              <img src={"/img/stoke.png"} />
                            ) : null}
                            {item.State === "BlackFriday" ? (
                              <img src={"/img/blackFriday.png"} />
                            ) : null}
                          </div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </Slider>
          </div>
        )}
        <div className="d-flex justify-content-center position-absolute button_container">
          <Link href={`/catalog`}>
            <div>
              <button className="banner_catalog">Каталог</button>
            </div>
          </Link>
        </div>
      </div>
    </section>
  );
};

export default Banner;

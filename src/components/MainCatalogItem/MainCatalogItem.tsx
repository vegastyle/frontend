import Router from 'next/router';
import React from 'react';
import {resizeImage, TYPE_IMAGE} from "../../helpers/resizeImaga";

const MainCatalogItem = ({name, imgPath, id}: {name: string, imgPath:{ext: string,hash: string, url: string}, id: number}) => {

    const goPage = () => {
        Router.push({
            pathname: '/catalog',
            query: {typeId: id.toString()},
        }).then(r =>{})

    }
    return (
        <div className="col-lg-4 col-md-4 col-sm-6 col-10" onClick={goPage}>
            <div className="block">
                <img src={resizeImage(TYPE_IMAGE.MEDIUM, imgPath)} alt=""/>
                <p>{name}</p>
                <button className="more_btn" >Подробнее</button>

            </div>
        </div>

    )
}


export default MainCatalogItem

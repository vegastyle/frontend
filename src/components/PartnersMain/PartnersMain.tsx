import Link from "next/link";
import React, { FC } from "react";
import OutgoingBlock from "src/utils/OutgoingBlock";
import { api } from "../../constant/api";

const PartnersMain: FC<{ title: string; img: string; text: string }> = ({
  title,
  img,
  text,
}) => {
  return (
    <div className="info_block mt-5">
      {/* <div className="sklad_block PartnersMain_bg" />
      <img src={`/img/coop_full.png`} alt="" />
      <div className="sklad_left PartnersMain_bg-content">
        <h3>{title}</h3>
        <div className="completed_left p-0">
          <p>{text}</p>
        </div>
        <Link href={"/cooperation"}>
          <a className={"sklad-link"}>Подробнее</a>
        </Link>
      </div> */}
      <div className="info_block_left">
        <div>
          <div className="info_block_left_background">
            <OutgoingBlock
              url="/img/coop_full.png"
              title="Сотрудничество"
              text="Подробнее"
              link="/cooperation"
            />
          </div>
        </div>
      </div>
      <div className="info_block_right">
        <div className="info_block_right_background">
          <OutgoingBlock
            url="/img/deliviry_full.png"
            title="Доставка"
            text="Подробнее"
            link="/order"
          />
        </div>
      </div>
    </div>
  );
};

export default PartnersMain;

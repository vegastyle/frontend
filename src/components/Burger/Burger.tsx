import React from "react";
import Hamburger from "hamburger-react";
import Image from "next/image";

import Link from "next/link";
import { useRouter } from "next/router";
import { useBurger } from "../../helpers/burgerContext";
import { navConfig } from "@components/Header/Header.config";

export const Burger = () => {
  const { route, replace } = useRouter();

  const { open, toggle } = useBurger();
  const goPage = (path) => {
    replace(`/${path}`).then(() => toggle());
  };
  return (
    <div className={`burger ${open ? "active" : ""}`}>
      {/* <div
        style={{
          position: "absolute",
          right: "20px",
          top: "20px",
          height: "50px",
          width: "50px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Hamburger color={"red"} toggled={open} toggle={toggle} />
      </div> */}
      <div className="d-flex justify-content-center">
        <img
          src={"https://vegastyle58.ru/api/uploads/Logo_caa5b25c99.jpg"}
          className="burger-img"
        />
      </div>
      <div className="line-white" />
      {navConfig.map((item) => {
        return (
          <div
            key={item.id}
            className={`burger-item ${route === item.path ? "active" : ""}`}
            onClick={() => goPage(item.path)}
          >
            {item.name}
          </div>
        );
      })}
      <div className="line-white" />
      <Link href={"tel: 33-02-33"}>
        <div className="d-flex align-items-center justify-content-center">
          <Image
            className={"header-main-phone-icon d-block"}
            src={"/img/red-phone.svg"}
            width={30}
            height={30}
          />
        </div>
      </Link>
    </div>
  );
};

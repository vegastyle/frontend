import Link from "next/link";
import React, { useEffect, useState } from "react";
// import MapModal from "@components/MapModal/MapModal";
import { fetcher } from "../../helpers/fether";
import { api } from "../../constant/api";
import { useRouter } from "next/router";
// import { Placemark, YMaps, Map } from "react-yandex-maps";

const Footer = () => {
  const [modalIsOpen, setIsOpen] = React.useState(false);
  const { pathname } = useRouter();
  const [footerData, setFooterData] = useState(null);

  useEffect(() => {
    fetcher(`${api}/footer`).then((res) => {
      setFooterData(res);
    });
  }, []);

  const close = () => {
    setIsOpen(false);
  };

  const open = () => {
    setIsOpen(true);
  };

  if (!footerData) {
    return null;
  }

  return (
    <>
      <div className={`footer ${pathname === "/" ? "m-0" : ""}`}>
        <div className="container">
          <div className="row">
            <div className="col-md-2 " />
            <div className="col-md-4 text-center text-sm-left">
              <div>
                <div className="d-flex justify-content-center ">
                  <img
                    src={"/img/LogoFooter.jpg"}
                    alt="Logo"
                    className="footer-logo"
                  />
                </div>
                <ul className="footer_menu">
                  <li>
                    <Link href={"/about"}>
                      <a>О нас</a>
                    </Link>
                  </li>
                  <li>
                    <Link href={"/calatog"}>
                      <a>Каталог</a>
                    </Link>
                  </li>
                  <li>
                    <Link href={"/reviews"}>
                      <a>Отзывы</a>
                    </Link>
                  </li>
                  <li>
                    <Link href={"/contacts"}>
                      <a>Контакты</a>
                    </Link>
                  </li>
                  <li>
                    <Link href={"/order"}>
                      <a>Доставка</a>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
            {/* <div
              className="col-md-4 d-none d-sm-block cursor-pointer"
              onClick={open}
            >
              <div className="map">
                <YMaps>
                  <Map
                    defaultState={{
                      center: [footerData.longitude, footerData.latitude],
                      zoom: 9,
                    }}
                  />
                </YMaps>
              </div>
            </div> */}
            <div className="col-md-4 md-mt1 text-center text-sm-left d-flex justify-content-center">
              <div className="soc">
                <div
                  className="d-flex justify-content-center"
                  style={{ padding: "0 0 0 8px" }}
                >
                  <h3>Следите за нами</h3>
                </div>
                <div className="d-flex justify-content-center follow_us">
                  {footerData && (
                    <Link href={footerData ? footerData.vk : ""}>
                      <div className="footer_icons">
                        <svg
                          id="youtube"
                          enableBackground="new 0 0 24 24"
                          height="32"
                          viewBox="0 0 24 24"
                          width="32"
                          xmlns="http://www.w3.org/2000/svg"
                          style={{ margin: "0.5rem" }}
                        >
                          <path
                            fill="black"
                            d="M19.915 13.028c-.388-.49-.277-.708 0-1.146.005-.005 3.208-4.431 3.538-5.932l.002-.001c.164-.547 0-.949-.793-.949h-2.624c-.668 0-.976.345-1.141.731 0 0-1.336 3.198-3.226 5.271-.61.599-.892.791-1.225.791-.164 0-.419-.192-.419-.739V5.949c0-.656-.187-.949-.74-.949H9.161c-.419 0-.668.306-.668.591 0 .622.945.765 1.043 2.515v3.797c0 .832-.151.985-.486.985-.892 0-3.057-3.211-4.34-6.886-.259-.713-.512-1.001-1.185-1.001H.9c-.749 0-.9.345-.9.731 0 .682.892 4.073 4.148 8.553C6.318 17.343 9.374 19 12.154 19c1.671 0 1.875-.368 1.875-1.001 0-2.922-.151-3.198.686-3.198.388 0 1.056.192 2.616 1.667C19.114 18.217 19.407 19 20.405 19h2.624c.748 0 1.127-.368.909-1.094-.499-1.527-3.871-4.668-4.023-4.878z"
                          ></path>
                        </svg>
                      </div>
                    </Link>
                  )}
                  {footerData.instagramm && (
                    <Link href={footerData.instagramm}>
                      <div className="footer_icons">
                        <svg
                          id="intagram"
                          enableBackground="new 0 0 24 24"
                          height="32"
                          viewBox="-20 -20 213.063 213.063"
                          width="32"
                          xmlns="http://www.w3.org/2000/svg"
                          style={{ margin: "0.5rem" }}
                        >
                          <path
                            d="M122.406 0H46.654C20.929 0 0 20.93 0 46.655v75.752c0 25.726 20.929 46.655 46.654 46.655h75.752c25.727 0 46.656-20.93 46.656-46.655V46.655C169.063 20.93 148.133 0 122.406 0zm31.657 122.407c0 17.455-14.201 31.655-31.656 31.655H46.654C29.2 154.063 15 139.862 15 122.407V46.655C15 29.201 29.2 15 46.654 15h75.752c17.455 0 31.656 14.201 31.656 31.655v75.752z"
                            fill="black"
                          ></path>
                          <path
                            d="M84.531 40.97c-24.021 0-43.563 19.542-43.563 43.563 0 24.02 19.542 43.561 43.563 43.561s43.563-19.541 43.563-43.561c0-24.021-19.542-43.563-43.563-43.563zm0 72.123c-15.749 0-28.563-12.812-28.563-28.561 0-15.75 12.813-28.563 28.563-28.563s28.563 12.813 28.563 28.563c0 15.749-12.814 28.561-28.563 28.561zM129.921 28.251c-2.89 0-5.729 1.17-7.77 3.22a11.053 11.053 0 0 0-3.23 7.78c0 2.891 1.18 5.73 3.23 7.78 2.04 2.04 4.88 3.22 7.77 3.22 2.9 0 5.73-1.18 7.78-3.22 2.05-2.05 3.22-4.89 3.22-7.78 0-2.9-1.17-5.74-3.22-7.78-2.04-2.05-4.88-3.22-7.78-3.22z"
                            fill="black"
                          ></path>
                        </svg>
                      </div>
                    </Link>
                  )}
                  {/* {footerData.instagramm && ( */}
                  {/* <Link href={footerData.instagramm}> */}
                  <Link
                    href={
                      "https://www.youtube.com/channel/UCDzb7AAYvF6s6OdD4jKg4QA/featured"
                    }
                  >
                    <div className="footer_icons">
                      <svg
                        id="youtube"
                        enableBackground="new 0 0 24 24"
                        height="32"
                        viewBox="0 0 512 512"
                        width="32"
                        xmlns="http://www.w3.org/2000/svg"
                        style={{ margin: "0.5rem" }}
                      >
                        <path
                          d="M490.24 113.92c-13.888-24.704-28.96-29.248-59.648-30.976C399.936 80.864 322.848 80 256.064 80c-66.912 0-144.032.864-174.656 2.912-30.624 1.76-45.728 6.272-59.744 31.008C7.36 138.592 0 181.088 0 255.904v.256c0 74.496 7.36 117.312 21.664 141.728 14.016 24.704 29.088 29.184 59.712 31.264C112.032 430.944 189.152 432 256.064 432c66.784 0 143.872-1.056 174.56-2.816 30.688-2.08 45.76-6.56 59.648-31.264C504.704 373.504 512 330.688 512 256.192v-.16-.096c0-74.848-7.296-117.344-21.76-142.016zM192 352V160l160 96-160 96z"
                          fill="black"
                          stroke="grey"
                        ></path>
                      </svg>
                    </div>
                  </Link>
                  {/* )} */}
                </div>
                <div className="col-md-12 text-center text-sm-left footer-contacts">
                  <div className="contact d-flex justify-content-center align-items-center flex-column">
                    <h3>Контакты</h3>
                    <p>{footerData.address}</p>

                    <div className="tab-phone cursor-pointer">
                      {/* <img src="/img/icons/phone.svg" alt="" /> */}
                      <svg
                        width="18px"
                        height="18px"
                        clip-rule="evenodd"
                        fill="#777777;"
                        fill-rule="evenodd"
                        image-rendering="optimizeQuality"
                        shape-rendering="geometricPrecision"
                        text-rendering="geometricPrecision"
                        version="1.1"
                        viewBox="0 0 333 333"
                        // xml:space="preserve"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <defs></defs>
                        <path
                          // class="fil0"
                          d="m55 29s12-7 17-3 37 65 37 65 3 7-1 14c-5 7-20 31-20 31s-1 7 4 16c8 12 77 81 90 89 9 6 16 4 16 4s23-15 30-19c7-5 14-1 14-1s61 31 65 36c4 6-3 17-3 17-18 20-49 37-76 28-21-8-44-26-64-44-34-30-63-59-93-93-18-20-36-43-43-63-10-28 7-59 27-77zm112 82c9 0 16 7 16 15 0 9-7 16-16 16-8 0-15-7-15-16 0-8 7-15 15-15zm92 0c8 0 15 7 15 15 0 9-7 16-15 16-9 0-15-7-15-16 0-8 6-15 15-15zm-46 0c9 0 15 7 15 15 0 9-6 16-15 16-8 0-15-7-15-16 0-8 7-15 15-15z"
                        ></path>
                      </svg>
                      <span className="ml-2">8 (800) 101-99-58</span>
                    </div>
                    <div className="tab-phone cursor-pointer">
                      {/* <img src="/img/icons/phone.svg" alt="" /> */}
                      <svg
                        width="18px"
                        height="18px"
                        clip-rule="evenodd"
                        fill="#777777;"
                        fill-rule="evenodd"
                        image-rendering="optimizeQuality"
                        shape-rendering="geometricPrecision"
                        text-rendering="geometricPrecision"
                        version="1.1"
                        viewBox="0 0 333 333"
                        // xml:space="preserve"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <defs></defs>
                        <path
                          // class="fil0"
                          d="m55 29s12-7 17-3 37 65 37 65 3 7-1 14c-5 7-20 31-20 31s-1 7 4 16c8 12 77 81 90 89 9 6 16 4 16 4s23-15 30-19c7-5 14-1 14-1s61 31 65 36c4 6-3 17-3 17-18 20-49 37-76 28-21-8-44-26-64-44-34-30-63-59-93-93-18-20-36-43-43-63-10-28 7-59 27-77zm112 82c9 0 16 7 16 15 0 9-7 16-16 16-8 0-15-7-15-16 0-8 7-15 15-15zm92 0c8 0 15 7 15 15 0 9-7 16-15 16-9 0-15-7-15-16 0-8 6-15 15-15zm-46 0c9 0 15 7 15 15 0 9-6 16-15 16-8 0-15-7-15-16 0-8 7-15 15-15z"
                        ></path>
                      </svg>
                      <span className="ml-2">8 (987) 077-07-34 </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <MapModal
        state={modalIsOpen}
        closeModal={close}
        latitude={footerData.latitude}
        long={footerData.longitude}
      /> */}
    </>
  );
};

export default Footer;

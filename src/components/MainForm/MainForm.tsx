import React, { FC } from "react";
import { api } from "../../constant/api";

type MainFormProps = {
  imgPath: string;
  title: string;
  description: string;
  changeDataForm: (key: string, value: string) => void;
  submitForm: () => void;
  value: { name: string; lastName: string; number: string; email: string };
  statusForm: { success: boolean; error: boolean };
};

const MainForm: FC<MainFormProps> = ({
  imgPath,
  title,
  description,
  changeDataForm,
  submitForm,
  value,
  statusForm,
}) => {
  return (
    <div
      className="magnet"
      style={{ backgroundImage: `url("/img/FeedBackForm.jpg")` }}
      id="form"
    >
      <div className="content">
        <h3>{title}</h3>
        <p>{description}</p>
        {statusForm.success && (
          <div className="alert alert-success" role="alert">
            Представитель свяжется с вами в ближайшее время!
          </div>
        )}
        <div className="form">
          <form onSubmit={(e) => e.preventDefault()}>
            <input
              type="text"
              placeholder="Имя"
              value={value.name}
              onChange={(e) => {
                const { value } = e.target;
                changeDataForm("name", value);
              }}
            />
            <input
              type="text"
              placeholder="Фамилия"
              value={value.lastName}
              onChange={(e) => {
                const { value } = e.target;
                changeDataForm("lastName", value);
              }}
            />
            <input
              type="email"
              placeholder="Email"
              value={value.email}
              onChange={(e) => {
                const { value } = e.target;
                changeDataForm("email", value);
              }}
            />
            <input
              type="tel"
              placeholder="Телефон"
              value={value.number}
              onChange={(e) => {
                const { value } = e.target;
                changeDataForm("number", value);
              }}
            />
            <button onClick={submitForm}>ЗАКАЗАТЬ</button>
          </form>
        </div>
      </div>
    </div>
  );
};
export default MainForm;

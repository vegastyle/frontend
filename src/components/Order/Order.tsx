import Link from "next/link";
import React, {FC} from "react"
import {resizeImage, TYPE_IMAGE} from "../../helpers/resizeImaga";

type OrderProps = {
    imgPath: any
    title: string
}

const Order:FC<OrderProps> = ({imgPath, title}) => {

    return (
        <div className="ship">
            <div className="container">
                <div className="ship_block d-flex justify-content-center align-items-center flex-sm-row flex-column">
                    <div className="left">
                        <h3>{title}</h3>
                        <Link href={"/order"}>
                            <a>Подробнее</a>
                        </Link>

                    </div>
                    <div className="right">
                        <img src={resizeImage(TYPE_IMAGE.MEDIUM, imgPath)} alt=""/>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default Order

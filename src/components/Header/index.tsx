import React, { FC, useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import NavBar from "@components/NavBar";
import { navConfig } from "@components/Header/Header.config";
import { fetcher } from "../../helpers/fether";
import { api } from "../../constant/api";
import AnchorLink from "react-anchor-link-smooth-scroll";
import { useFavorite } from "../../helpers/favoriteContext";
import { resizeImage, TYPE_IMAGE } from "../../helpers/resizeImaga";
import Hamburger from "hamburger-react";
import { useBurger } from "../../helpers/burgerContext";
type HeaderProps = {};

const Header: FC<HeaderProps> = () => {
  const [headerData, setHeaderData] = useState(null);
  const [catalogItems, setCatalogItems] = useState([]);
  const [active, setActive] = useState(Boolean);
  const { setDataFunc, clearAll } = useFavorite();
  const { toggle, open } = useBurger();
  const router = useRouter();

  useEffect(() => {
    if (window) {
      const data = JSON.parse(localStorage.getItem("favorite"));
      if (data) {
        setDataFunc(data);
      }
    }
    fetcher(`${api}/header`).then((res) => setHeaderData(res));
  }, []);

  const { data, deleteItemFromFavorite } = useFavorite();
  const [catalog, setCatalog] = useState();
  const [dropDownMap, setDropDownMap] = useState(false);

  useEffect(() => {
    fetch(`${api}/tests`)
      .then((response) => response.json())
      .then((divans) => setCatalogItems(divans));
    fetch(`${api}/detail-items`)
      .then((response) => response.json())
      .then((divans) => setCatalog(divans));
  }, []);

  if (!catalog && !headerData) {
    return null;
  }

  const dataFavorites = (catalog || []).filter((el) => data.includes(el.id));

  const newNavConfig = navConfig.map((item) => {
    if (item.name === "Каталог") {
      return {
        ...item,
        children: catalogItems.map((el) => {
          return {
            id: el.id,
            name: el.type,
            path: `catalog?typeId=${el.id}`,
          };
        }),
      };
    }
    return item;
  });

  return (
    <div className={`container-fluid header-fix header-shadow`}>
      <div
        className="d-flex justify-content-between align-items-center"
        style={{ background: "white" }}
      >
        <Link href="/">
          <a className="header__logo">
            <img src={`${api}${headerData.logo.url}`} alt="" />
          </a>
        </Link>

        <NavBar config={newNavConfig} activeRoute={router.route} />
        <div className="header__favorite d-flex align-items-center position-relative">
          <Link href={"/favorite"}>
            <div className="d-flex align-items-center">
              <svg
                id="color"
                enableBackground="new 0 0 24 24"
                height="18"
                viewBox="0 0 24 24"
                width="18"
                xmlns="http://www.w3.org/2000/svg"
                style={{ margin: "0 5px 0 0" }}
              >
                <path
                  d="m23.363 8.584-7.378-1.127-3.307-7.044c-.247-.526-1.11-.526-1.357 0l-3.306 7.044-7.378 1.127c-.606.093-.848.83-.423 1.265l5.36 5.494-1.267 7.767c-.101.617.558 1.08 1.103.777l6.59-3.642 6.59 3.643c.54.3 1.205-.154 1.103-.777l-1.267-7.767 5.36-5.494c.425-.436.182-1.173-.423-1.266z"
                  fill={
                    (catalog || []).filter((el) => data.includes(el.id))
                      .length > 0
                      ? "#C2811D"
                      : "white"
                  }
                  stroke="#C2811D"
                />
              </svg>
              <span>Избранное</span>
            </div>
          </Link>
          {(catalog || []).filter((el) => data.includes(el.id)).length > 0 && (
            <div>
              <div className="favorite-hover-block">
                {dataFavorites.map((el) => {
                  return (
                    <a
                      key={el.id}
                      className="favorite-block-item d-flex flex-column position-relative"
                    >
                      <div
                        className="favorite-block-img"
                        style={{
                          backgroundImage: `url(${resizeImage(
                            TYPE_IMAGE.SMALL,
                            el.images[0]
                          )})`,
                        }}
                      />
                      <div className="favorite-block-name">{el.name}</div>
                      <div className="favorite-block-price">{`${
                        el.price || 0
                      } р.`}</div>
                      <div
                        className="close"
                        onClick={() => deleteItemFromFavorite(el.id)}
                      >
                        <svg
                          width="18"
                          height="18"
                          viewBox="0 0 18 18"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M1 1L9 9L17 1"
                            stroke="#023838"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M17 17L9 9L1 17"
                            stroke="#023838"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                      </div>
                    </a>
                  );
                })}
                {dataFavorites.length > 0 && (
                  <div className="favorite-clear" onClick={clearAll}>
                    Очистить
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="header__location d-flex align-items-center">
          <Image
            height={18}
            width={24}
            src="/img/icons/location.svg"
            alt="LocationIcons"
          />
          <span onClick={() => setDropDownMap((s) => !s)}>Кузнецк</span>
          <div
            className={`${
              dropDownMap ? "d-flex" : "d-none"
            } flex-column dropDownMap`}
          >
            <div className="dropDownMap-title">Открыть карту в новом окне?</div>
            <div className="d-flex justify-content-around">
              <a
                href={`https://yandex.ru/maps/?pt=${headerData.long},${headerData.lat}&z=17`}
                onClick={() => setDropDownMap(false)}
                target={"_blank"}
                className="dropDownMap-btn"
              >
                Да
              </a>
              <div
                className="dropDownMap-btn"
                onClick={() => setDropDownMap((s) => !s)}
              >
                Нет
              </div>
            </div>
          </div>
        </div>
        <div className="header__phone d-flex align-items-center flex-column">
          <div className="d-flex">
            <img
              src="/img/icons/phone.svg"
              alt="LocationIcons"
              style={{ width: "20px" }}
            />

            <Link href={`tel:${headerData.number}`}>
              <span>{headerData.number}</span>
            </Link>
          </div>

          <div className="header-time-work">
            <span>{headerData.timeWork}</span>
          </div>
        </div>
        <AnchorLink href="#form">
          <div className="header__call js-header-call">Обратный звонок</div>
        </AnchorLink>
        <div className="mobile_call">
          <a>
            <img src="/img/icons/btn_call.svg" alt="" />
          </a>
        </div>
        <div className={"burger-btn"} onClick={toggle}>
          <Hamburger color={"green"} toggled={open} />
        </div>
      </div>
    </div>
  );
};
export async function getServerSideProps() {
  const mainPageData = await fetcher(`${api}/main-page-2`);
  return {
    props: {
      mainPageData,
    },
  };
}

export default Header;

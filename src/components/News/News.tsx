import Link from "next/link";
import React, { FC } from "react";
import { resizeImage, TYPE_IMAGE } from "../../helpers/resizeImaga";

type NewsProps = {
  title: string;
  dataMainNews: {
    id: number;
    title: string;
    updated_at: string;
    bg: { url: string };
    imageBg: { ext: string; hash: string; url: string };
  }[];
  custom_news: {
    id: number;
    title: string;
    description: string;
    updated_at: string;
    image: { ext: string; hash: string; url: string };
  }[];
};

const News: FC<NewsProps> = ({ dataMainNews, custom_news }) => {
  return (
    <div>
      <div className={"d-flex flex-wrap flex-md-nowrap news-programm"}>
        {dataMainNews.map((item, index) => {
          return (
            <div
              className="news-block-big d-flex justify-content-center"
              key={index}
            >
              <img
                src={resizeImage(TYPE_IMAGE.MEDIUM, item.imageBg)}
                alt=""
                style={{ width: "auto", height: "100%" }}
              />
              <div className="data">
                <h3>{item.title}</h3>
                {/* <p>{item.updated_at}</p> */}
                <button className="news-block-button">
                  <Link href={`/news/${item.id}`}>
                    <a>Подробнее</a>
                  </Link>
                </button>
              </div>
            </div>
          );
        })}
      </div>

      <div className={" overflow-auto news-block-info"}>
        {custom_news.map((item, index) => {
          console.log(item);
          return (
            <div className="news_custom_block" key={index}>
              <Link href={`/options/${item.id}`}>
                <a>{item.title}</a>
              </Link>
              {/* <div className="data_mini">
                <h3>{item.description}</h3>
              </div> */}
            </div>
          );
        })}
      </div>
      <a href="#" className="news-link d-sm-none d-block">
        Смотреть еще
      </a>
    </div>
  );
};
export default News;

import Link from "next/link";
import React, { FC } from "react";
import Slider from "react-slick";
import { resizeImage, TYPE_IMAGE } from "../../helpers/resizeImaga";

type SckladProgrammProps = {
  title: string;
  imgPath: { ext: string; hash: string; url: string }[];
  description: string;
};

const settings = {
  dots: false,
  infinite: true,
  arrows: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  swipeToSlide: true,
  loop: true,
  autoPlay: true,
  margin: 10,
  vertical: false,
  autoplaySpeed: 2000,
  autoplay: true,
};

const SkladProgramm: FC<SckladProgrammProps> = ({
  title,
  description,
  imgPath,
}) => {
  return (
    <div className="completed_project">
      <div className="container-fluid">
        <div className="row justify-content-lg-between justify-content-center align-items-center">
          <div className="col-lg-5 col-md-10 col-12">
            <div className="completed_left scroll-block d-flex flex-column">
              <h3>{title}</h3>
              <p>{description}</p>
              <div>
                <Link href={"/project"}>
                  <a>Подробнее</a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-7 col-md-10">
            <div className="swiper-container">
              <div className="swiper-wrapper">
                <Slider {...settings}>
                  {imgPath.map((item, index) => {
                    return (
                      <img
                        key={index}
                        src={resizeImage(TYPE_IMAGE.MEDIUM, item)}
                        alt=""
                      />
                    );
                  })}
                  {imgPath.map((item, index) => {
                    return (
                      <img
                        key={index}
                        src={resizeImage(TYPE_IMAGE.MEDIUM, item)}
                        alt=""
                      />
                    );
                  })}
                </Slider>
              </div>
            </div>

            <div className="swiper-button-prev"></div>
            <div className="swiper-button-next"></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SkladProgramm;

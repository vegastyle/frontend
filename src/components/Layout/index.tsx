import React from "react";
import Header from "@components/Header";
import Footer from "@components/Footer";
import { Burger } from "@components/Burger/Burger";

const Layout = ({ children }) => {
  return (
    <div>
      <div style={{ height: "70px" }}></div>
      <div>
        <Burger />
        <Header />
      </div>
      <div>{children}</div>

      <Footer />
    </div>
  );
};

export default Layout;

import React, { FC, useEffect, useState } from "react";

// import { View, Text } from 'react-native';
import { useRouter } from "next/router";
import Link from "next/link";

export const BreadCrumbsConfig = {
  "/about": "О нас",
  "/": "Главная",
  "/catalog": "Каталог",
  "/project": "Реализованные проекты",
  "/reviews": "Отзывы",
  "/contacts": "Контакты",
  "/order": "Доставка",
  "/favorite": "Избранное",
  "/cooperation": "Сотрудничество",
};

const convertBreadcrumb = (value) => {
  return BreadCrumbsConfig[value];
};

type Props = {
  path?: {
    name: string;
    path: string;
  }[];
};

const Breadcrumbs: FC<Props> = ({ path }) => {
  const router = useRouter();
  const [breadcrumbs, setBreadcrumbs] = useState(null);

  useEffect(() => {
    if (router) {
      const linkPath = router.pathname.split("/");
      linkPath.shift();

      const pathArray = linkPath.map((path, i) => {
        return {
          breadcrumb: path,
          href: "/" + linkPath.slice(0, i + 1).join("/"),
        };
      });

      setBreadcrumbs(pathArray);
    }
  }, [router]);

  if (!breadcrumbs) {
    return null;
  }
  return (
    <ul className="breadCrumbs-custom d-flex">
      <li className={"breadCrumbs-custom-item"}>
        <a href="/">Главная</a>
      </li>
      {path &&
        path.map((item, i) => (
          <li
            className={`breadCrumbs-custom-item cursor-pointer ${
              i === breadcrumbs.length - 1 ? "active" : ""
            }`}
            key={i}
          >
            <Link key={item.name} href={`/${item.path}`}>
              <a>{item.name}</a>
            </Link>
          </li>
        ))}
      {!path &&
        breadcrumbs.map((breadcrumb, i) => {
          return (
            <li
              className={`breadCrumbs-custom-item ${
                i === breadcrumbs.length - 1 ? "active" : ""
              }`}
              key={breadcrumb.href}
            >
              <Link href={breadcrumb.href}>
                <a className={"header-nav-link"}>
                  {convertBreadcrumb(breadcrumb.href)}
                </a>
              </Link>
            </li>
          );
        })}
    </ul>
  );
};

export default Breadcrumbs;

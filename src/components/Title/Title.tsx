import React from 'react';

const Title = ({title}: {title: string}) => {
    return (
        <div className={"section_title_green"}>
            <h3>{title}</h3>
        </div>
    )
}


export default Title

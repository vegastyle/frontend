import React, { FC, useEffect, useState } from "react";
import Title from "@components/Title/Title";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import SmallBanner from "@components/SmallBanner";
import { Map, Placemark } from "react-yandex-maps";

type ContactsProps = {
  contactsData: {
    title: string;
    city: string;
    number: string;
    recvisit: string;
    address: string;
    workTime: string;
    bannerTitle: string;
    bannerDescription: string;
  };
};

const Contacts: FC<ContactsProps> = ({ contactsData }) => {
  const {
    city,
    address,
    number,
    recvisit,
    workTime,
    bannerTitle,
    bannerDescription,
  } = contactsData;

  const [footerData, setFooterData] = useState(null);

  useEffect(() => {
    fetcher(`${api}/footer`).then((res) => {
      setFooterData(res);
    });
  }, []);

  return (
    <div>
      <SmallBanner title={bannerTitle} description={bannerDescription} />

      <div className={"container"}>
        <Breadcrumbs />
        <Title title={"Котакты"} />
        <div className={"row"}>
          <div className="col-lg-5">
            <div className="d-flex mb-5 justify-content-between">
              <div className="d-flex flex-column">
                <div className="contacts-title">{city}</div>
                <div className="contacts-text">{number}</div>
              </div>
            </div>
            <div className="d-flex mb-5 align-items-center">
              <svg
                width="24"
                height="23"
                viewBox="0 0 24 23"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M23.9521 9.36749C23.8688 9.1903 23.6911 9.07694 23.4946 9.07694H21.9999C21.0497 3.9201 16.5214 0 11.0939 0C4.9771 0 0 4.9771 0 11.0939C0 17.2108 4.97659 22.1873 11.0939 22.1873C15.0416 22.1873 18.7232 20.0626 20.7019 16.6434C20.9807 16.1609 20.8163 15.5446 20.3332 15.2653C19.8512 14.9859 19.2344 15.1504 18.9561 15.6329C17.3364 18.4317 14.3237 20.1709 11.0939 20.1709C6.08925 20.1709 2.01698 16.0991 2.01698 11.0949C2.01698 6.08976 6.08925 2.01801 11.0939 2.01801C15.4047 2.01801 19.0163 5.04144 19.9365 9.07745H18.4536C18.257 9.07745 18.0798 9.19081 17.9961 9.368C17.9129 9.54519 17.9399 9.75404 18.0655 9.90467L20.587 12.9301C20.683 13.045 20.8245 13.1114 20.9746 13.1114C21.1242 13.1114 21.2662 13.0445 21.3617 12.9301L23.8831 9.90467C24.0088 9.75352 24.0353 9.54468 23.9521 9.36749Z"
                  fill="#303030"
                />
                <path
                  d="M11.0938 2.52148C10.5367 2.52148 10.0853 2.97288 10.0853 3.52998V9.64935C9.62935 9.96901 9.3291 10.496 9.3291 11.0944C9.3291 12.0677 10.1211 12.8597 11.0943 12.8597C11.3609 12.8597 11.6121 12.7958 11.8388 12.6891L15.3469 14.7138C15.5062 14.8062 15.6788 14.8496 15.8503 14.8496C16.1986 14.8496 16.5377 14.6688 16.7245 14.3451C17.0028 13.8636 16.8379 13.2462 16.3548 12.9679L12.8433 10.9402C12.7963 10.4071 12.519 9.94143 12.1033 9.65037V3.52998C12.1023 2.97339 11.6504 2.52148 11.0938 2.52148ZM11.0938 11.8502C10.6767 11.8502 10.3376 11.5106 10.3376 11.0944C10.3376 10.6778 10.6772 10.3382 11.0938 10.3382C11.5105 10.3382 11.8501 10.6778 11.8501 11.0944C11.8501 11.5106 11.5105 11.8502 11.0938 11.8502Z"
                  fill="#303030"
                />
              </svg>
              <div className="contacts-text ml-3">{`Рабочее время ${workTime}`}</div>
            </div>
            <div className="d-flex flex-column mb-5">
              <div className="contacts-title">Адрес</div>
              <div className="contacts-text">{address}</div>
            </div>
            <div className="d-flex flex-column mb-5">
              <div className="contacts-title">Реквизиты</div>
              <div className="contacts-text">{recvisit}</div>
            </div>
            <div className="contacts-btn">
              <a>Получить каталог</a>
            </div>
          </div>
          <div className="col-lg-6 Map">
            {footerData ? (
              <Map
                width={"100%"}
                height={"100%"}
                defaultState={{
                  center: [footerData.longitude, footerData.latitude],
                  zoom: 14,
                }}
              >
                <Placemark
                  geometry={[footerData.longitude, footerData.latitude]}
                />
              </Map>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const contactsData = await fetcher(`${api}/contacts`);
  return {
    props: {
      contactsData,
    },
  };
}

export default Contacts;

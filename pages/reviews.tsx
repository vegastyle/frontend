import React, {FC} from "react";
import Banner from "@components/Banner";
import Title from "@components/Title/Title";
import {api} from "../src/constant/api";
import {fetcher} from "../src/helpers/fether";
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import SmallBanner from "@components/SmallBanner";





type ReviewsProps = {

    data: {
        bannerTitle: string
        bannerDescription: string
        reviews: {
            name: string
            review: string
            published_at: string
        }[]
    }

}

const Reviews: FC<ReviewsProps> = ({data}) => {
    const {bannerDescription, bannerTitle, reviews} = data

    return (
        <div

        >
            <SmallBanner title={bannerTitle} description={bannerDescription} />

            <div className={"container"}>
                <Breadcrumbs />
                <Title title={"Отзывы"}/>
                <div className={"row"}>
                    {
                        reviews.map((item, index) => {
                            return (
                                <div key={index} className="col-lg-6 col-md-6 col-12">
                                    <div className="reviews-block w-100">
                                        <div className="reviews-name">{item.name}</div>
                                        <div className="reviews-date">{item.published_at}</div>
                                        <div className="reviews-text"> {item.review}
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }



                </div>
            </div>

        </div>
    );
};

export async function getServerSideProps() {
    const data = await fetcher(`${api}/review-page`)
    return {
        props: {
            data
        },
    }
}

export default Reviews;

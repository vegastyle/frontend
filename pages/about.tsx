import React from "react";
import AboutItem from "@components/AboutItem/AboutItem";
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import SmallBanner from "@components/SmallBanner";
import { resizeImage, TYPE_IMAGE } from "../src/helpers/resizeImaga";

const About: React.FC<any> = ({ aboutData }) => {
  return (
    <>
      <SmallBanner title={"О нас"} description={""} />
      <div className={"container"}>
        <Breadcrumbs />
        {aboutData.aboutItems.map((item, index) => {
          return (
            <AboutItem
              key={String(index)}
              index={index}
              imagePath={resizeImage(TYPE_IMAGE.MEDIUM, item.image)}
              reverse={index % 2 === 0}
              title={item.title}
              description={item.description}
            />
          );
        })}
      </div>
    </>
  );
};

export async function getServerSideProps() {
  const aboutData = await fetcher(`${api}/about`);

  return {
    props: {
      aboutData: aboutData,
    },
  };
}

export default About;

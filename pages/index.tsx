import React, { FC, useState } from "react";
import Banner, { Banner_item_type } from "@components/Banner";
import Title from "@components/Title/Title";
import MainCatalogItem from "@components/MainCatalogItem/MainCatalogItem";
import News from "@components/News/News";
import Order from "@components/Order/Order";
import MainForm from "@components/MainForm/MainForm";
import PartnersMain from "@components/PartnersMain/PartnersMain";
import SkladProgramm from "@components/SckladProgramm/SckladProgramm";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import { resizeImage, TYPE_IMAGE } from "../src/helpers/resizeImaga";
import Options from "./options/[id]";

type HomeProps = {
  mainPageData: {
    bannerTitle: string;
    cooperText: string;
    bannerDescription: string;
    titleForTypeCatalog: string;
    bannerSlider: string[];
    catalogs: any[];
    sklad_programm: {
      title: string;
      description: string;
      text: string;
      sliderImage: { id: string; ext: string; hash: string; url: string }[];
    };
    newsTitle: string;
    news_data: {
      id: number;
      title: string;
      updated_at: string;
      bg: { url: string };
      imageBg: { ext: string; hash: string; url: string };
    }[];
    custom_news: {
      id: number;
      title: string;
      updated_at: string;
      description: string;
      image: { ext: string; hash: string; url: string };
    }[];
    orderImage: { url: string };
    orderTitle: string;
    formTitle: string;
    formDescription: string;
    formBackgroundImage: { url: string };
    partnerTitle: string;
    partnerImage: { ext: string; hash: string; url: string };
    banner_item: { Banner_items: Banner_item_type[] };
  };
  catalogData: any[];
  oneItem: any[];
  catalogsData: any[];
};

const Home: FC<HomeProps> = ({ mainPageData }) => {
  const [formData, setFormData] = useState({
    name: "",
    lastName: "",
    number: "",
    email: "",
  });
  const [statusForm, setStatusForm] = useState({
    success: false,
    error: false,
  });
  const sendForm = () => {
    if (formData.number && formData.name) {
      fetch(`${api}/feedback-forms`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      }).then(() => {
        setFormData({ name: "", lastName: "", number: "", email: "" });
        setStatusForm((s) => ({
          ...s,
          success: true,
        }));
        setTimeout(() => {
          setStatusForm((s) => ({
            ...s,
            success: false,
          }));
        }, 3000);
      });
    }
  };

  const changeDataForm = (key, value) => {
    setFormData((s) => {
      return {
        ...s,
        [key]: value,
      };
    });
  };

  const {
    bannerTitle,
    bannerSlider,
    cooperText,
    bannerDescription,
    titleForTypeCatalog,
    catalogs,
    sklad_programm,
    formBackgroundImage,
    formDescription,
    formTitle,
    newsTitle,
    orderImage,
    orderTitle,
    partnerTitle,
    custom_news,
    news_data,
    partnerImage,
    banner_item,
  } = mainPageData;
  return (
    <div>
      <div className="banner_main_page">
        {/* <a href="/catalog"> */}
        <Banner
          title={bannerTitle}
          description={bannerDescription}
          sliders={bannerSlider}
          Banner_item={banner_item.Banner_items}
        />
        {/* </a> */}
      </div>
      <div className={"container"}>
        <Title title={titleForTypeCatalog} />
        <div className={"row justify-content-center"}>
          {catalogs.map((item, index) => {
            return (
              <MainCatalogItem
                key={index}
                name={item.type}
                imgPath={item.image}
                id={item.id}
              />
            );
          })}
        </div>
      </div>
      <PartnersMain
        title={partnerTitle}
        img={resizeImage(TYPE_IMAGE.MEDIUM, partnerImage)}
        text={cooperText}
      />
      <SkladProgramm
        title={sklad_programm.title}
        description={sklad_programm.description}
        imgPath={sklad_programm.sliderImage}
      />
      <News
        title={newsTitle}
        custom_news={custom_news}
        dataMainNews={news_data}
      />

      {/* <Order title={orderTitle} imgPath={orderImage} /> */}
      <div style={{ margin: "120px 0 0 0" }}>
        <MainForm
          statusForm={statusForm}
          value={formData}
          submitForm={sendForm}
          changeDataForm={changeDataForm}
          imgPath={formBackgroundImage.url}
          title={formTitle}
          description={formDescription}
        />
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const mainPageData = await fetcher(`${api}/main-page-2`);
  return {
    props: {
      mainPageData,
    },
  };
}

export default Home;

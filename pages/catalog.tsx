import React, { useEffect, useState } from "react";
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import { useRouter } from "next/router";
import { api } from "../src/constant/api";
import CatalogItem from "@components/CatalogItem/CatalogItem";
import SmallBanner from "@components/SmallBanner";
import { useFavorite } from "../src/helpers/favoriteContext";

type Props = {
  catalogData: any;
  catalogContentData: any;
};

const Catalog: React.FC<Props> = ({ catalogData, catalogContentData }) => {
  const router = useRouter();
  const { addItemToFavoriteStore, data } = useFavorite();

  const [queryParams, setQueryParams] = useState(
    router.query.typeId ? router.query.typeId : "1"
  );

  catalogData.test;
  const updateQuery = (newQuery) => {
    router
      .push({
        pathname: "/catalog",
        query: { typeId: encodeURI(newQuery) },
      })
      .then((r) => {});
  };

  useEffect(() => {
    setQueryParams(router.query.typeId ? router.query.typeId : "1");
  }, [router.query.typeId]);

  useEffect(() => {
    updateQuery(queryParams);

    // const content = catalogData.find((el) => el.id.toString() === queryParams);

    serCurrentContent(null);
  }, [queryParams]);

  const [currentContent, serCurrentContent] = useState(
    catalogData.find((el) => el.id.toString() === queryParams)
  );
  useEffect(() => {
    const content = catalogData.find((el) => el.id.toString() === queryParams);

    serCurrentContent(content);
  }, [currentContent]);

  return (
    <>
      <SmallBanner
        title={catalogContentData.bannerTitle}
        description={catalogContentData.bannerDescription}
      />
      <section className="catalog-list">
        <div className="container">
          <ul className="catalog-list-tabs " role="tablist">
            {catalogData
              .sort((a, b) => a.id - b.id)
              .map((item, index) => {
                return (
                  <li
                    key={index}
                    onClick={() => setQueryParams(item.id.toString())}
                    className="catalog-list-tabs-item"
                  >
                    <a
                      className={`nav-link ${
                        item.id.toString() === queryParams ? "active" : ""
                      }`}
                      data-toggle="tab"
                      role="tab"
                    >
                      {item.type}
                    </a>
                  </li>
                );
              })}
          </ul>
        </div>
      </section>
      <div className={"container"}>
        <Breadcrumbs />
      </div>

      <div className="container">
        <div className="row justify-content-md-start justify-content-center">
          {currentContent
            ? currentContent.detail_items.map((item, index) => {
                return (
                  <CatalogItem
                    key={index}
                    item={item}
                    active={data.includes(item.id)}
                    addFavorite={addItemToFavoriteStore}
                  />
                );
              })
            : null}
        </div>
      </div>
    </>
  );
};

export async function getServerSideProps() {
  const res = await fetch(`${api}/tests`);
  const catalogContent = await fetch(`${api}/catalog-content`);
  const mainPage = await fetch(`${api}/main-page-2`);

  const catalogData = await res.json();
  const catalogContentData = await catalogContent.json();
  const mainPageData = await mainPage.json();

  return {
    props: {
      catalogData,
      catalogContentData,
      mainPageData,
    },
  };
}

export default Catalog;

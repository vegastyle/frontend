import React, { FC } from "react";
import { fetcher } from "../../src/helpers/fether";
import { api } from "../../src/constant/api";
import Title from "@components/Title/Title";

type OptionsProps = {
  data?: {
    title: string;
    description: string;
    date: string;
  };
};

const Options: React.FC<OptionsProps> = ({ data }) => {
  const { title, description } = data;
  console.log(data);
  console.log(`${api}/custom-news-1-s/`);
  return (
    <div className={"container"}>
      <Title title={title} />
      <div className="row justify-content-center">
        <div className="col-lg-10">
          <span>{description}</span>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps({ params, preview = null }) {
  const data = await fetcher(`${api}/custom-news-1-s/${params.id}`);
  if (!data) {
  }

  return {
    props: {
      preview,
      data,
    },
  };
}

export default Options;

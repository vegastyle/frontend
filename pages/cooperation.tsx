import React, { FC, useState } from "react";
import Banner from "@components/Banner";
import Title from "@components/Title/Title";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import SmallBanner from "@components/SmallBanner";
import MainForm from "@components/MainForm/MainForm";

type ReviewsProps = {
  data: {
    title: string;
    bannerTitle: string;
    bannerDescription: string;
    formBackgroundImage: { url: string };
    formTitle: string;
    formDescription: string;
    text: string;
  };
};

const Cooperation: FC<ReviewsProps> = ({ data }) => {
  const [formData, setFormData] = useState({
    name: "",
    lastName: "",
    number: "",
    email: "",
  });
  const [statusForm, setStatusForm] = useState({
    success: false,
    error: false,
  });

  const sendForm = () => {
    if (formData.number && formData.name) {
      fetch(`${api}/feedback-forms`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      }).then(() => {
        setFormData({ name: "", lastName: "", number: "", email: "" });
        setStatusForm((s) => ({
          ...s,
          success: true,
        }));
        setTimeout(() => {
          setStatusForm((s) => ({
            ...s,
            success: false,
          }));
        }, 3000);
      });
    }
  };

  const changeDataForm = (key, value) => {
    setFormData((s) => ({
      ...s,
      [key]: value,
    }));
  };

  return (
    <div>
      <SmallBanner
        title={data.bannerTitle}
        description={data.bannerDescription}
      />

      <div className={"container"}>
        <Breadcrumbs />
        <Title title={"Сотрудничество"} />
        <div className="completed_left p-0">
          <p>{data.text}</p>
        </div>
      </div>
      <div style={{ margin: "120px 0 0 0" }}>
        <MainForm
          statusForm={statusForm}
          value={formData}
          submitForm={sendForm}
          changeDataForm={changeDataForm}
          imgPath={data.formBackgroundImage.url}
          title={data.formTitle}
          description={data.formDescription}
        />
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const data = await fetcher(`${api}/cooper`);
  return {
    props: {
      data,
    },
  };
}

export default Cooperation;

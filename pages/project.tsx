import React, { FC } from "react";
import Banner from "@components/Banner";
import Title from "@components/Title/Title";
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import SmallBanner from "@components/SmallBanner";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import Link from "next/link";
import { resizeImage, TYPE_IMAGE } from "src/helpers/resizeImaga";

type ProjectsProps = {
  bannerDescription: string;
  bannerTitle: string;
  projects: {
    id: string;
    name: string;
    price: number;
    images: { url: string; hash: string; ext: string }[];
  }[];
};

const Projects: FC<ProjectsProps> = ({
  projects,
  bannerDescription,
  bannerTitle,
}) => {
  return (
    <div>
      <SmallBanner title={bannerTitle} description={bannerDescription} />

      <div className={"container"}>
        <Breadcrumbs />
        <Title title={"Реализованные проекты"} />
        <div className={"row justify-content-center"}>
          {projects.map((el) => {
            return (
              <div
                key={el.id}
                className="project-item col-lg-3 col-md-5 col-sm-8 col-10 mb-5"
              >
                <div
                  className="project-bg"
                  style={{
                    backgroundImage: `url(${resizeImage(
                      TYPE_IMAGE.MEDIUM,
                      el.images[0]
                    )})`,
                  }}
                />

                <div className="project-hover-block">
                  <div className="project-item-name">{el.name}</div>
                  <div className="project-item-price">{el.price}</div>
                  <div className="project-item-btn">
                    <Link href={`/detail/${el.id}`}>
                      <a href="#">Подробнее</a>
                    </Link>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const projects = await fetcher(`${api}/project`);
  return {
    props: {
      bannerDescription: projects.bannerDescription,
      bannerTitle: projects.bannerTitle,
      projects: projects.detail_items,
    },
  };
}

export default Projects;

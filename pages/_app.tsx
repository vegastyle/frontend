import React from "react";
import { AppProps } from "next/app";
import "@styles/global.scss";
import "@styles/required.scss";
import "../node_modules/slick-carousel/slick/slick.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "react-fancybox/lib/fancybox.css";
import Layout from "@components/Layout";
import { QueryClient, QueryClientProvider } from "react-query";
import { YMaps } from "react-yandex-maps";
import { FavoriteContext } from "src/helpers/favoriteContext";
import NProgress from "nprogress"; //nprogress module
import "nprogress/nprogress.css";
import "react-awesome-lightbox/build/style.css";
import { Router } from "next/router";
import Head from "next/head";
import { YMInitializer } from "react-yandex-metrika";
import { ContextBugger } from "../src/helpers/burgerContext";
import ReactGA from "react-ga";

const queryClient = new QueryClient();

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  // ReactGA.initialize("UA-212582502-1", { debug: true });
  // ReactGA.pageview(window.location.pathname + window.location.search);
  ReactGA.initialize("UA-212582502-1", { debug: true });
  return (
    <>
      <Head>
        <title>Фабрика мягкой мебели VegaStyle</title>
        <meta name="author" content="PavelPanin" />
        <meta
          name="keywords"
          content="мягкая мебель, Кузнецк, диваны, угловые диваны, VegaStyle, мягкая мебель в Кузнецке, каталог и цены диванов, вега, вегастайл, диваны в Кузнецке, мебель Кузнецк, мебель в Кузнецке, vega style, вега style, style, vega, мупф, мупфыенду, мупф ыефнду "
        />
        <meta name="robots" content="index, follow" />
        <meta name="description" content="VegaStyle" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="copyright" content="VegaStyle”" />
        <meta property="og:title" content="Фабрика мягкой мебели VegaStyle" />
        <meta
          property="og:site_name"
          content="Фабрика мягкой мебели VegaStyle"
        />
        <meta property="og:url" content="https://vegastyle58.ru/" />
        <meta
          property="og:description"
          content="Фабрика мягкой мебели VegaStyle"
        />
        <meta property="og:type" content="profile" />
        <link
          rel="sitemap"
          type="application/xml"
          title="Sitemap"
          href="/sitemap.xml"
        />
      </Head>
      <YMInitializer accounts={[79316659]} options={{ webvisor: true }} />
      <YMInitializer accounts={[86599511]} options={{ webvisor: true }} />

      <QueryClientProvider client={queryClient}>
        <ContextBugger>
          <FavoriteContext data={[]}>
            <YMaps>
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </YMaps>
          </FavoriteContext>
        </ContextBugger>
      </QueryClientProvider>
    </>
  );
}

export default MyApp;

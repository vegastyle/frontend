import React, {FC} from "react"
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import {fetcher} from "../src/helpers/fether";
import {api} from "../src/constant/api";



type OrderProps = {
    orderData: {
        title: string
        description: string
        image: {url:string}
    }
}

const Order:FC<OrderProps> = ({orderData}) => {

    const {title, description, image} = orderData
    return (
        <>

            <section>
                <div className="container">
                    <Breadcrumbs />
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="order-title mb-5">{title}</div>
                            <div className="order-text">
                                {description}
                            </div>
                            <a className="order-btn" href="#">Получить прайс-лист</a>
                        </div>
                        <div className="col-lg-6">
                            <div className="order-img" style={{
                                backgroundImage: `url(${api}${image.url})`
                            }}/>
                        </div>
                    </div>
                </div>
            </section>
        </>

    )
}

export async function getServerSideProps() {
    const orderData = await fetcher(`${api}/order`)
    return {
        props: {
            orderData
        },
    }
}

export default Order

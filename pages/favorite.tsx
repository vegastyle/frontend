import React from "react";
import { useFavorite } from "../src/helpers/favoriteContext";
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import SmallBanner from "@components/SmallBanner";
import FavoriteItem from "@components/FavoriteItem/FavoriteItem";
import { api } from "../src/constant/api";

const Favorite = ({ catalogData, resFavorite }) => {
  const { data } = useFavorite();
  return (
    <>
      <SmallBanner title={resFavorite.bannerTitle} description={resFavorite.bannerDiscription} />
      <div className={"container"}>
        <Breadcrumbs />
        <div className="row">
          {catalogData
            .filter((el) => data.includes(el.id))
            .map((item, index) => {
              return (
                <FavoriteItem
                  key={index}
                  item={item}
                />
              );
            })}
        </div>
      </div>
    </>
  );
};

export async function getServerSideProps() {
  const res = await fetch(`${api}/detail-items`);
  const resFavorite = await fetch(`${api}/favorite-content`);

  const catalogData = await res.json();
  const resFavoriteData = await resFavorite.json();
  return {
    props: {
      catalogData,
      resFavorite: resFavoriteData
    },
  };
}

export default Favorite;

import React, { FC, useEffect, useState } from "react";
import { api } from "../../src/constant/api";
import { fetcher } from "../../src/helpers/fether";
import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs";
import Slider from "react-slick";
import SmallBanner from "@components/SmallBanner";
import { useFavorite } from "../../src/helpers/favoriteContext";
import { resizeImage, TYPE_IMAGE } from "../../src/helpers/resizeImaga";
import Lightbox from "react-awesome-lightbox";
import Link from "next/link";
// import { BuyNowButton } from "@ecwid/nextjs-ecwid-plugin";
import BuyModal from "src/components/BuyModal/BuyModal";

const settings = {
  dots: false,
  infinite: true,
  arrows: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  swipeToSlide: true,
  loop: false,
  autoPlay: true,
  margin: 10,
  vertical: false,
  autoplaySpeed: 2000,
  autoplay: true,
};
type DetailProps = {
  data: {
    id: number;
    name: string;
    mecaName: string;
    description: string;
    price: number;
    images: { url: string; ext: string; hash: string }[];
    sliderItems: any[];
    titleSlider: string;
    animImage: { url: string; ext: string; hash: string };
    characteristics: {
      id: string;
      title: string;
      info: { id: string; option: string; value: string }[];
    }[];
    schemaDivan: { url: string; ext: string; hash: string };
    dimensions: {
      size: {
        height: string;
        long: string;
        width: string;
      };
      sleepingPlace: {
        height: string;
        long: string;
        width: string;
      };
    };
  };
  mainPageData: any;
};

const Detail: FC<DetailProps> = ({ data, mainPageData }) => {
  const [favoriteFlag, setFavoriteFlag] = useState(false);
  const [mainImage, setMainImage] = useState({ ext: "", hash: "", url: "" });
  const [currentImage, setCurrentImage] = useState("");
  const [openModal, setOpenModal] = useState(false);
  if (!data) {
    return <div>not Found</div>;
  }

  const {
    data: favoriteData,
    addItemToFavoriteStore,
    deleteItemFromFavorite,
  } = useFavorite();

  const {
    name,
    description,
    price,
    images,
    characteristics,
    sliderItems,
    titleSlider,
    schemaDivan,
    id,
  } = data;

  useEffect(() => {
    setMainImage(images[0]);
  }, []);

  return (
    <>
      <SmallBanner title={name} description={""} />
      <div className={"container"}>
        <Breadcrumbs
          path={[
            { path: "catalog", name: "Каталог" },
            { path: `detail/${id}`, name: name },
          ]}
        />
        <section>
          <BuyModal
            mainPageData={mainPageData}
            isOpen={openModal}
            setOpenModal={setOpenModal}
          />
          <div className="container">
            <div className="row flex-column flex-lg-row align-items-center ">
              {/* <BuyNowButton
                storeId="60959772"
                productId="418686788"
                isShowPrice={false}
              /> */}
              <div className="col-lg-8 col-12 ">
                <div className="row mb-5">
                  <div className="col-12">
                    {currentImage && (
                      <Lightbox
                        onClose={() => setCurrentImage("")}
                        image={currentImage}
                        title="Image Title"
                      />
                    )}
                    {mainImage && (
                      <div
                        className="detail-main-img"
                        onClick={() =>
                          setCurrentImage(
                            resizeImage(TYPE_IMAGE.LARGE, mainImage)
                          )
                        }
                        style={{
                          backgroundImage: `url(${resizeImage(
                            TYPE_IMAGE.LARGE,
                            mainImage
                          )})`,
                        }}
                      ></div>
                    )}
                  </div>
                </div>
                <div className="row mb-5 ">
                  {images.map((item, index) => {
                    return (
                      <div
                        className="col-3 mb-5"
                        key={`${index}_char123`}
                        onClick={() => setMainImage(item)}
                      >
                        <div className="detail-small-img">
                          {/*<img width={100}  height={100} src={resizeImage(TYPE_IMAGE.MEDIUM, item)} alt=""/>*/}
                          <div
                            className="detail-main-img"
                            style={{
                              backgroundImage: `url(${resizeImage(
                                TYPE_IMAGE.MEDIUM,
                                item
                              )})`,
                            }}
                          />
                        </div>
                      </div>
                    );
                  })}
                  <div className="col-12 description">
                    <div className="detail-main-description-title mt-5">
                      ОПИСАНИЕ
                    </div>
                    {description ? (
                      <div className="detail-main-description-text text-md ">
                        {description}
                      </div>
                    ) : (
                      <div>Описание скоро появится</div>
                    )}
                  </div>
                  <div className="col-12">
                    <div className="d-flex flex-column">
                      {data.animImage && (
                        <div
                          className={
                            "d-flex align-item-center justify-content-center w-100 mt-4 gif"
                          }
                        >
                          <img
                            src={resizeImage(TYPE_IMAGE.MEDIUM, data.animImage)}
                            alt=""
                          />
                        </div>
                      )}
                      {data.mecaName && (
                        <div className="d-flex mb-3 align-items-center">
                          <div className="detail-description-title mr-1 mb-0">
                            Механизм
                          </div>
                          <div className="detail-description-text">
                            {data.mecaName}
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 d-flex d-lg-block flex-column col-md-10 align-items-center  ">
                <div className="col-md-12 col-lg-12">
                  {/* <div className="detail-title">{name}</div> */}
                  {/* <div className="detail-price">{`${
                    price || "Цена не указана"
                  } `}</div> */}
                  <div
                    className={`detail-btn mb-3 `}
                    onClick={() => {
                      if (favoriteData.includes(data.id)) {
                        deleteItemFromFavorite(data.id);
                      } else {
                        addItemToFavoriteStore(data.id);
                      }
                    }}
                  >
                    <a
                      className={`${
                        favoriteData.includes(data.id) ? "active" : ""
                      }`}
                    >
                      {/* <svg
                        width="25"
                        height="24"
                        viewBox="0 0 25 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M24.1424 7.92606C23.7924 7.61827 23.3616 7.41984 22.8964 7.35225L17.2013 6.52471L14.6544 1.36409C14.4463 0.942599 14.1245 0.594095 13.7236 0.35638C13.3517 0.135884 12.9255 0.0192871 12.491 0.0192871C12.0566 0.0192871 11.6303 0.135842 11.2585 0.35638C10.8576 0.594095 10.5358 0.942557 10.3277 1.36409L7.78082 6.52475L2.08572 7.35225C1.62054 7.41984 1.18967 7.61827 0.839717 7.92606C0.515073 8.21158 0.272543 8.58091 0.138271 8.99415C0.00403941 9.40734 -0.0168318 9.8487 0.0779392 10.2705C0.180138 10.7252 0.412086 11.139 0.748722 11.4671L4.86973 15.4841L3.89687 21.1562C3.77633 21.8591 3.96982 22.5737 4.42774 23.1168C4.88906 23.6641 5.56217 23.9779 6.27445 23.9779C6.66237 23.9779 7.05062 23.8816 7.39722 23.6993L12.4911 21.0213L17.5849 23.6993C17.9315 23.8816 18.3197 23.9779 18.7077 23.9779C19.4199 23.9779 20.093 23.6641 20.5544 23.1168C21.0123 22.5737 21.2058 21.8591 21.0853 21.1562L20.1125 15.4841L24.2335 11.4671C24.5701 11.139 24.802 10.7252 24.9042 10.2705C24.999 9.8487 24.9782 9.40734 24.8439 8.99415C24.7096 8.58095 24.467 8.21162 24.1424 7.92606ZM22.9926 10.1942L18.4414 14.6306C18.2918 14.7765 18.2235 14.9866 18.2589 15.1925L19.3333 21.4567C19.4036 21.8671 19.0776 22.2003 18.7077 22.2003C18.61 22.2003 18.5094 22.1771 18.4121 22.126L12.7865 19.1685C12.6941 19.1198 12.5926 19.0955 12.4911 19.0955C12.3897 19.0955 12.2882 19.1198 12.1957 19.1685L6.57009 22.126C6.47287 22.1771 6.37212 22.2003 6.27453 22.2003C5.90457 22.2003 5.57856 21.8671 5.64893 21.4567L6.72332 15.1925C6.75867 14.9866 6.69038 14.7765 6.54075 14.6306L1.98946 10.1942C1.61266 9.82696 1.82058 9.18705 2.34128 9.11136L8.63094 8.19743C8.8377 8.16739 9.01646 8.03752 9.10895 7.85013L11.9218 2.15076C12.0382 1.91483 12.2646 1.79686 12.4911 1.79686C12.7175 1.79686 12.9439 1.91487 13.0603 2.15076L15.8732 7.85013C15.9657 8.03747 16.1444 8.16739 16.3512 8.19743L22.6408 9.11136C23.1615 9.18705 23.3694 9.82696 22.9926 10.1942Z"
                          fill="#0F7C7C"
                        />
                        <path
                          d="M18.7089 24C18.3176 24 17.926 23.9028 17.5765 23.719L12.4923 21.0461L7.40812 23.719C7.05854 23.9029 6.66696 24 6.27568 24C5.55726 24 4.87839 23.6835 4.41308 23.1316C3.95122 22.5837 3.75607 21.863 3.87765 21.154L4.84864 15.4928L0.735435 11.4833C0.395936 11.1524 0.162038 10.7351 0.0589265 10.2765C-0.0366743 9.85099 -0.0156372 9.40585 0.119756 8.98913C0.25519 8.57242 0.499836 8.19989 0.82726 7.91192C1.18025 7.60147 1.61485 7.40135 2.08401 7.33317L7.76832 6.50721L10.3104 1.35634C10.5203 0.931195 10.8449 0.579746 11.2492 0.339956C11.6243 0.117551 12.0541 0 12.4924 0C12.9305 0 13.3604 0.117551 13.7355 0.339956C14.1398 0.579705 14.4644 0.931195 14.6743 1.35634L17.2164 6.50721L22.9007 7.33317C23.3699 7.40131 23.8044 7.60143 24.1574 7.91192C24.4849 8.19989 24.7295 8.57242 24.8649 8.98918C25.0003 9.40594 25.0214 9.85108 24.9257 10.2765C24.8227 10.7351 24.5887 11.1524 24.2492 11.4834L20.136 15.4928L21.107 21.154C21.2285 21.863 21.0334 22.5837 20.5715 23.1316C20.1061 23.6834 19.4272 24 18.7089 24ZM12.4923 20.9993L12.5019 21.0043L17.5958 23.6823C17.9394 23.863 18.3243 23.9585 18.7088 23.9585C19.4149 23.9585 20.0823 23.6473 20.5397 23.1048C20.9936 22.5663 21.1855 21.8579 21.066 21.1611L20.0913 15.4782L24.2202 11.4536C24.5539 11.1283 24.7838 10.7181 24.8851 10.2673C24.9791 9.84917 24.9585 9.41158 24.8254 9.00191C24.6922 8.59225 24.4518 8.22607 24.1299 7.943C23.783 7.63786 23.3558 7.44114 22.8946 7.37417L17.1887 6.54509L17.1839 6.5353L14.637 1.37459C14.4308 0.956672 14.1117 0.61124 13.7143 0.375557C13.3456 0.156928 12.9231 0.0413689 12.4923 0.0413689C12.0616 0.0413689 11.639 0.156928 11.2703 0.375557C10.8729 0.611198 10.5539 0.956672 10.3476 1.37459L7.79583 6.54501L7.78504 6.54658L2.08995 7.37413C1.62879 7.44114 1.20161 7.63782 0.854646 7.94296C0.532823 8.22603 0.292327 8.59217 0.159216 9.00187C0.0261052 9.41154 0.0054415 9.84908 0.0994241 10.2673C0.200751 10.7181 0.430708 11.1283 0.764439 11.4536L4.89324 15.4782L4.89142 15.489L3.91856 21.1611C3.79906 21.8579 3.99089 22.5663 4.44487 23.1048C4.90225 23.6473 5.56954 23.9585 6.27572 23.9585C6.66028 23.9585 7.04518 23.8629 7.38882 23.6823L12.4923 20.9993Z"
                          fill="#0F7C7C"
                        />
                      </svg> */}
                      <span>{`${
                        favoriteData.includes(data.id)
                          ? "В ИЗБРАННОМ"
                          : "ДОБАВИТЬ И ИЗБРАННОЕ"
                      }`}</span>
                    </a>
                  </div>
                  <div className="detail-btn mb-5 button_buy">
                    <button type="button" onClick={() => setOpenModal(true)}>
                      {/* <svg
                      width="25"
                      height="24"
                      viewBox="0 0 25 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M24.1424 7.92606C23.7924 7.61827 23.3616 7.41984 22.8964 7.35225L17.2013 6.52471L14.6544 1.36409C14.4463 0.942599 14.1245 0.594095 13.7236 0.35638C13.3517 0.135884 12.9255 0.0192871 12.491 0.0192871C12.0566 0.0192871 11.6303 0.135842 11.2585 0.35638C10.8576 0.594095 10.5358 0.942557 10.3277 1.36409L7.78082 6.52475L2.08572 7.35225C1.62054 7.41984 1.18967 7.61827 0.839717 7.92606C0.515073 8.21158 0.272543 8.58091 0.138271 8.99415C0.00403941 9.40734 -0.0168318 9.8487 0.0779392 10.2705C0.180138 10.7252 0.412086 11.139 0.748722 11.4671L4.86973 15.4841L3.89687 21.1562C3.77633 21.8591 3.96982 22.5737 4.42774 23.1168C4.88906 23.6641 5.56217 23.9779 6.27445 23.9779C6.66237 23.9779 7.05062 23.8816 7.39722 23.6993L12.4911 21.0213L17.5849 23.6993C17.9315 23.8816 18.3197 23.9779 18.7077 23.9779C19.4199 23.9779 20.093 23.6641 20.5544 23.1168C21.0123 22.5737 21.2058 21.8591 21.0853 21.1562L20.1125 15.4841L24.2335 11.4671C24.5701 11.139 24.802 10.7252 24.9042 10.2705C24.999 9.8487 24.9782 9.40734 24.8439 8.99415C24.7096 8.58095 24.467 8.21162 24.1424 7.92606ZM22.9926 10.1942L18.4414 14.6306C18.2918 14.7765 18.2235 14.9866 18.2589 15.1925L19.3333 21.4567C19.4036 21.8671 19.0776 22.2003 18.7077 22.2003C18.61 22.2003 18.5094 22.1771 18.4121 22.126L12.7865 19.1685C12.6941 19.1198 12.5926 19.0955 12.4911 19.0955C12.3897 19.0955 12.2882 19.1198 12.1957 19.1685L6.57009 22.126C6.47287 22.1771 6.37212 22.2003 6.27453 22.2003C5.90457 22.2003 5.57856 21.8671 5.64893 21.4567L6.72332 15.1925C6.75867 14.9866 6.69038 14.7765 6.54075 14.6306L1.98946 10.1942C1.61266 9.82696 1.82058 9.18705 2.34128 9.11136L8.63094 8.19743C8.8377 8.16739 9.01646 8.03752 9.10895 7.85013L11.9218 2.15076C12.0382 1.91483 12.2646 1.79686 12.4911 1.79686C12.7175 1.79686 12.9439 1.91487 13.0603 2.15076L15.8732 7.85013C15.9657 8.03747 16.1444 8.16739 16.3512 8.19743L22.6408 9.11136C23.1615 9.18705 23.3694 9.82696 22.9926 10.1942Z"
                        fill="#c2811d"
                      />
                      <path
                        d="M18.7089 24C18.3176 24 17.926 23.9028 17.5765 23.719L12.4923 21.0461L7.40812 23.719C7.05854 23.9029 6.66696 24 6.27568 24C5.55726 24 4.87839 23.6835 4.41308 23.1316C3.95122 22.5837 3.75607 21.863 3.87765 21.154L4.84864 15.4928L0.735435 11.4833C0.395936 11.1524 0.162038 10.7351 0.0589265 10.2765C-0.0366743 9.85099 -0.0156372 9.40585 0.119756 8.98913C0.25519 8.57242 0.499836 8.19989 0.82726 7.91192C1.18025 7.60147 1.61485 7.40135 2.08401 7.33317L7.76832 6.50721L10.3104 1.35634C10.5203 0.931195 10.8449 0.579746 11.2492 0.339956C11.6243 0.117551 12.0541 0 12.4924 0C12.9305 0 13.3604 0.117551 13.7355 0.339956C14.1398 0.579705 14.4644 0.931195 14.6743 1.35634L17.2164 6.50721L22.9007 7.33317C23.3699 7.40131 23.8044 7.60143 24.1574 7.91192C24.4849 8.19989 24.7295 8.57242 24.8649 8.98918C25.0003 9.40594 25.0214 9.85108 24.9257 10.2765C24.8227 10.7351 24.5887 11.1524 24.2492 11.4834L20.136 15.4928L21.107 21.154C21.2285 21.863 21.0334 22.5837 20.5715 23.1316C20.1061 23.6834 19.4272 24 18.7089 24ZM12.4923 20.9993L12.5019 21.0043L17.5958 23.6823C17.9394 23.863 18.3243 23.9585 18.7088 23.9585C19.4149 23.9585 20.0823 23.6473 20.5397 23.1048C20.9936 22.5663 21.1855 21.8579 21.066 21.1611L20.0913 15.4782L24.2202 11.4536C24.5539 11.1283 24.7838 10.7181 24.8851 10.2673C24.9791 9.84917 24.9585 9.41158 24.8254 9.00191C24.6922 8.59225 24.4518 8.22607 24.1299 7.943C23.783 7.63786 23.3558 7.44114 22.8946 7.37417L17.1887 6.54509L17.1839 6.5353L14.637 1.37459C14.4308 0.956672 14.1117 0.61124 13.7143 0.375557C13.3456 0.156928 12.9231 0.0413689 12.4923 0.0413689C12.0616 0.0413689 11.639 0.156928 11.2703 0.375557C10.8729 0.611198 10.5539 0.956672 10.3476 1.37459L7.79583 6.54501L7.78504 6.54658L2.08995 7.37413C1.62879 7.44114 1.20161 7.63782 0.854646 7.94296C0.532823 8.22603 0.292327 8.59217 0.159216 9.00187C0.0261052 9.41154 0.0054415 9.84908 0.0994241 10.2673C0.200751 10.7181 0.430708 11.1283 0.764439 11.4536L4.89324 15.4782L4.89142 15.489L3.91856 21.1611C3.79906 21.8579 3.99089 22.5663 4.44487 23.1048C4.90225 23.6473 5.56954 23.9585 6.27572 23.9585C6.66028 23.9585 7.04518 23.8629 7.38882 23.6823L12.4923 20.9993Z"
                        fill="#c2811d"
                      />
                    </svg> */}
                      <span>
                        {price || "ЦЕНА НЕ УКАЗАНА"}
                        {price && "₽"}
                      </span>
                    </button>
                  </div>
                </div>
                <div className="detail-description col-md-10 col-lg-12 ">
                  {characteristics.map((item, index) => {
                    return (
                      <div
                        key={`${index}_char000`}
                        className="detail-description-item"
                      >
                        <div className="detail-description-title col-12">
                          <div className="aa"></div>
                          {item.title}
                        </div>
                        {item.info.map((el, index) => {
                          return (
                            <div
                              key={`${index}_char`}
                              className="d-flex mb-4 col-12 "
                            >
                              <div
                                className={`detail-description-text-bold  ${
                                  el.value ? "col-5" : "col-12 text-center"
                                }`}
                              >
                                {el.option}
                              </div>
                              {el.value && (
                                <div className="detail-description-text col-7">
                                  {el.value}
                                </div>
                              )}
                            </div>
                          );
                        })}
                      </div>
                    );
                  })}
                </div>
              </div>

              {schemaDivan && (
                <div
                  className="col-6"
                  onClick={() =>
                    setCurrentImage(resizeImage(TYPE_IMAGE.MEDIUM, schemaDivan))
                  }
                >
                  {schemaDivan && (
                    <img
                      className={"mw-100"}
                      src={resizeImage(TYPE_IMAGE.MEDIUM, schemaDivan)}
                      alt="shema"
                    />
                  )}
                </div>
              )}
            </div>
          </div>
        </section>
        <section>
          <div className="section_title_green  mb-5 mb-5">
            <h3>{titleSlider}</h3>
          </div>
          <div className="about-carousel ">
            <Slider {...settings}>
              {sliderItems.map((el, index) => {
                return (
                  <div
                    key={index * 10}
                    className="carousel-custom-item detail-carousel-item d-flex flex-column align-items-center justify-content-center h-100"
                  >
                    <img
                      src={resizeImage(TYPE_IMAGE.MEDIUM, el.images[0])}
                      alt="detailSlider"
                    />
                    <span>{el.name}</span>
                    <Link href={`/detail/${el.id}`}>
                      <button>Перейти</button>
                    </Link>
                  </div>
                );
              })}
            </Slider>
          </div>
        </section>
      </div>
    </>
  );
};

export async function getServerSideProps({ params, preview = null }) {
  const mainPage = await fetch(`${api}/main-page-2`);

  const data = await fetcher(`${api}/detail-items/${params.id}`);
  if (!data) {
  }

  const mainPageData = await mainPage.json();

  return {
    props: {
      mainPageData,
      preview,
      data,
    },
  };
}

export default Detail;

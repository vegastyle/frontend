const withPlugins = require('next-compose-plugins');
const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');
const withFonts = require('next-fonts');



const config = {
};

module.exports =withFonts(withCSS(withSass())) ;
